<?php

if (isset($_POST['num1']) && isset($_POST['num2'])) {

	$num1 = (float)trim($_POST['num1']);
	$num2 = (float)trim($_POST['num2']);
	$operation = $_POST['operation'];

	$result;

	switch ($operation) {
		case '+':
			$result = $num1 + $num2;
			break;

		case '-':
			$result = $num1 - $num2;
			break;

		case '*':
			$result = $num1 * $num2;
			break;

		case '/':
			if($num2 == 0) {
				echo 'На ноль делить нельзя!';
			}
			$result = $num1 / $num2;
			break;		
	}

	echo $result;

}
